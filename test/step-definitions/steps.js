/* global process */
//Cucumber
const {
    Given,
    Then,
    And,
    When
} = require('cucumber');
//Nightwatch
const browser = require('nightwatch-api').client;
//Chai
const expect = require('chai').expect;
const assert = require('chai').assert;
const should = require('chai').should();

var username = "USER_NAME"; 
var password;

if (process.env.ABAP_DEVELOPMENT_USER === undefined) {
    //let config = require("./../config");
    username = "USER_NAME";
    password = "USER_PASSWORD";
} else {
    username = process.env.ABAP_DEVELOPMENT_USER;
    password = process.env.ABAP_DEVELOPMENT_PASSWORD;
}

//load QAS env
async function loadUrl(server, client) {
    if (server === 'proxy') server = 'server';
    await browser.url(`your-testing-url`);
}

//load PROD env
async function loadUrlProd(server, client) {
    if (server === 'proxy') server = 'server';
    await browser.url(`your-testing-url`);
}

//Given steps are used to describe the initial context of the system
Given('I want to open process direct on {word} client {int}', async function(server, client) {
    await loadUrl(server, client);
});

Given('I open HANA on {word} client {int}', async function(server, client) {
    await loadUrl(server, client);
});

Given('I open HANA', async function() {
    await browser.url('URL');
});

Given('I open HANA PROD Login', async function() {
    await browser.url('URL'); 
});

Given('I open HANA QAS', async function() {
    await loadUrl();
});

Given('I open HANA PROD', async function() {
    await loadUrlProd();
});
/* - - - - - */


//login to hana system
Then('I click the T4 Button', async function() {
    await browser.waitForElementPresent(`#__item2-button`, 50);
    await browser.click(`#__item2-button`);
}); 

Then('I click the Quality Button', async function() {
    await browser.waitForElementPresent(`#__item13-button`, 50);
    await browser.click(`#__item13-button`);
}); 

Then('I click the Launchpad Button', async function() {
    await browser.waitForElementPresent(`#__item18-__list1-__xmlview1--links-1-8-content`, 60); 
    await browser.click(`#__item18-__list1-__xmlview1--links-1-8-content`);
});

Then('I login', async function() {
    await browser.url('URL');
    await browser.waitForElementPresent(`input#xs_username-inner`, 60);
    await browser.click(`input#xs_username-inner`);
    await browser.setValue(`input#xs_username-inner`, process.env.NWUsername);
    await browser.setValue(`input#xs_password-inner`, process.env.NWPassword);
    await browser.click(`#logon_button-content`);
    await browser.pause(1000);
});

Then('I login to PROD', async function() {
    await browser.url('URL');
    await browser.waitForElementPresent(`input#xs_username-inner`, 60);
    await browser.click(`input#xs_username-inner`);
    await browser.setValue(`input#xs_username-inner`, process.env.NWUsernameP);
    await browser.setValue(`input#xs_password-inner`, process.env.NWPasswordP);
    await browser.click(`#logon_button-content`);
    await browser.pause(1000);
});

//When steps are used to describe an event, or an action.
When('I click the {word} tile', async function(tile) {
    await browser.waitForElementPresent(`#__tile5-title-inner`, 60);
    await browser.pause(1000);
    await browser.click(`#__tile5.OneByOne`);
});

Then('I should see a login failure', async function() {
    await browser.expect.element('#LOGIN_ERROR_BLOCK').to.be.present;
});

Then('I should see the Fiori launchpad', async function() {
    await browser.expect.element(
        ".sapUshellTile :first-Child[title~=Inbox]").to.be.present.after(60);
});

Then('I should see the login screen', async function() {
    await browser.expect.element('#__tile4-title-inner').to.be.present; //check for EM Tile
    await browser.pause(1000); //add delay because HANA is slow
});

//Then steps are used to describe an expected outcome, or result.
Then('I should see the inbox screen', async function() {
    await browser.waitForElementPresent('h1', 50000); //check ABSTUDY header 
    await browser.pause(1000); //add browser pause, unittest failed overnight 
    await browser.expect.element('#__filter0-__xmlview0--gogoFragment--header-0-text').text.to.contain('Report'); //check header text
});

When('I click the Outstanding button', async function() {
    await browser.waitForElementPresent('#__filter0-__xmlview0--gogoFragment--header-1', 50000); //Wait for Outstanding itin 
    await browser.click(`#__filter0-__xmlview0--gogoFragment--header-1`); //change report to oustanding itin
});

When('I click the Outstanding button QAS', async function() {
    await browser.waitForElementPresent('#__filter1-__xmlview1--gogoFragment--header-1', 50000); //Wait for Outstanding itin 
    await browser.click(`#__filter0-__xmlview0--gogoFragment--header-1`); //change report to oustanding itin
});

/* /////////////////////////////////////
    Check all Reports load data in PROD
*/ /////////////////////////////////////

Then('PROD Outstanding Report should display results', async function() {
    await browser.pause(1600); // pause for prod having more DATA
    await browser.waitForElementPresent('#outstandingTravelItineraries--outstandingTravelItinerariesReportTable-triggerInfo', 990000); //Wait for Out-Report Results
    await browser.expect.element('#outstandingTravelItineraries--outstandingTravelItinerariesReportTable-triggerInfo').text.to.contain('[ 10 / '); 
    //check results is loading data by checking there is more than 10 to load
}); 

Then('PROD Emergency Report should display results', async function() {
    await browser.pause(1200); // pause for prod
    await browser.waitForElementPresent('#em--emReportTable-triggerInfo', 90000); //Wait for Out-Report Results
    await browser.expect.element('#em--emReportTable-triggerInfo').text.to.contain('[ 10 / '); 
    //check results is loading data by checking there is more than 10 to load
}); 

Then('PROD Reimbursement Report should display results', async function() {
    await browser.pause(1200); // pause for prod
    await browser.waitForElementPresent('#reimbursement--reimbursementReportTable-triggerInfo', 90000); //Wait for Out-Report Results
    await browser.expect.element('#reimbursement--reimbursementReportTable-triggerInfo').text.to.contain('[ 10 / '); 
    //check results is loading data by checking there is more than 10 to load
}); 

Then('PROD Quality Report should display results', async function() {
    await browser.pause(1200); // pause for prod
    await browser.waitForElementPresent('#qualityCheck--qualityCheckReportTable-triggerInfo', 90000); //Wait for Out-Report Results
    await browser.expect.element('#qualityCheck--qualityCheckReportTable-triggerInfo').text.to.contain('[ 10 / '); 
    //check results is loading data by checking there is more than 10 to load
}); 

Then('PROD No Show Report should display results', async function() {
    await browser.pause(1200); // pause for prod
    await browser.waitForElementPresent('#missedCosts--missedCostsReportTable-triggerInfo', 90000); //Wait for Out-Report Results
    await browser.expect.element('#missedCosts--missedCostsReportTable-triggerInfo').text.to.contain('[ 10 / '); 
    //check results is loading data by checking there is more than 10 to load
}); 

Then('PROD All TMA Report should display results', async function() {
    await browser.pause(1200); // pause for prod
    await browser.waitForElementPresent('#tma--tmaReportTable-triggerInfo', 90000); //Wait for Out-Report Results
    await browser.expect.element('#tma--tmaReportTable-triggerInfo').text.to.contain('[ 10 / '); 
    //check results is loading data by checking there is more than 10 to load
}); 

/* /////////////////////////////////////
    END Check all Reports load data in PROD
*/ /////////////////////////////////////

/* /////////////////////////////////////
    Check all Reports load data in QAS
*/ /////////////////////////////////////

Then('Outstanding Report should display results', async function() {
    await browser.pause(500); 
    await browser.waitForElementPresent('#outstandingTravelItineraries--outstandingTravelItinerariesReportTable-triggerInfo', 990000); //Wait for Out-Report Results
    await browser.expect.element('#outstandingTravelItineraries--outstandingTravelItinerariesReportTable-triggerInfo').text.to.contain('[ 10 / '); 
    //check results is loading data by checking there is more than 10 to load
}); 

Then('Emergency Report should display results', async function() {
    await browser.waitForElementPresent('#em--emReportTable-triggerInfo', 900); //Wait for Out-Report Results
    await browser.expect.element('#em--emReportTable-triggerInfo').text.to.contain('[ 10 / '); 
    //check results is loading data by checking there is more than 10 to load
}); 

Then('Reimbursement Report should display results', async function() {
    await browser.waitForElementPresent('#reimbursement--reimbursementReportTable-triggerInfo', 900); //Wait for Out-Report Results
    await browser.expect.element('#reimbursement--reimbursementReportTable-triggerInfo').text.to.contain('[ 10 / '); 
    //check results is loading data by checking there is more than 10 to load
}); 

Then('Quality Report should display results', async function() {
    await browser.pause(60000); // pause for prod
    await browser.waitForElementPresent('#qualityCheck--qualityCheckReportTable-triggerInfo', 900); //Wait for Out-Report Results
    await browser.expect.element('#qualityCheck--qualityCheckReportTable-triggerInfo').text.to.contain('[ 10 / '); 
    //check results is loading data by checking there is more than 10 to load
}); 

Then('No Show Report should display results', async function() {
    await browser.waitForElementPresent('#missedCosts--missedCostsReportTable-triggerInfo', 900); //Wait for Out-Report Results
    await browser.expect.element('#missedCosts--missedCostsReportTable-triggerInfo').text.to.contain('[ 10 / '); 
    //check results is loading data by checking there is more than 10 to load
}); 

Then('All TMA Report should display results', async function() {
    await browser.waitForElementPresent('#tma--tmaReportTable-triggerInfo', 900); //Wait for Out-Report Results
    await browser.expect.element('#tma--tmaReportTable-triggerInfo').text.to.contain('[ 10 / '); 
    //check results is loading data by checking there is more than 10 to load
}); 

/* /////////////////////////////////////
    END Check all Reports load data in QAS
*/ /////////////////////////////////////

Then('Outstanding Report should open', async function() {
    await browser.waitForElementPresent('#outstandingTravelItineraries--outstandingTravelItinerariesFilterBar-filterItem-___INTERNAL_-fEducation-bdi', 50000); //Wait for filter label text 
    await browser.expect.element('#outstandingTravelItineraries--outstandingTravelItinerariesReportTable-tblBody').text.to.not.contain('No data'); //check results table is not empty
});

When('I click the Reimbursement button', async function() {
    await browser.waitForElementPresent('#__filter0-__xmlview0--gogoFragment--header-2', 50000); //Wait for Reimb Tab
    await browser.click(`#__filter0-__xmlview0--gogoFragment--header-2`); //Click the reimb tab
});

Then('Reimbursement Report should open', async function() {
    await browser.waitForElementPresent('#reimbursement--reimbursementReportTable-tblBody', 30000); //Wait for filter label text 
    await browser.expect.element('#reimbursement--reimbursementReportTable-tblBody').text.to.not.contain('No data'); //check results table is not empty
});

When('I open No Show Report', async function(){
    await browser.waitForElementPresent('#__filter0-__xmlview0--gogoFragment--header-3', 50000); //Wait for no show Tab
    await browser.click(`#__filter0-__xmlview0--gogoFragment--header-3`); //Click the no show tab
});

Then('No Show Report should open', async function(){
    await browser.waitForElementPresent('#missedCosts--missedCostsReportTable-tblBody', 30000); //Wait for filter label text 
    await browser.expect.element('#missedCosts--missedCostsReportTable-tblBody').text.to.not.contain('No data'); //check results table is not empty;
});

When('I open Quality Report', async function(){
    await browser.waitForElementPresent('#__filter0-__xmlview0--gogoFragment--header-4', 50000); //Wait for quality Tab --- WILL FAIL WITHOUT ACCESS
    await browser.click(`#__filter0-__xmlview0--gogoFragment--header-4`); //Click the quality tab
});

Then('Quality Report should open', async function(){
    await browser.waitForElementPresent('#qualityCheck--qualityCheckReportTable-tblBody', 30000); //Wait for filter label text 
    await browser.expect.element('#qualityCheck--qualityCheckReportTable-tblBody').text.to.not.contain('No data'); //check results table is not empty;
});

When('I open Emergency Report', async function(){
    await browser.waitForElementPresent('#__filter0-__xmlview0--gogoFragment--header-0', 50000); //Wait for emergency Tab
    await browser.click(`#__filter0-__xmlview0--gogoFragment--header-0`); //Click the emergency tab
});

Then('the Emergency Report should open', async function(){
    await browser.waitForElementPresent('#em--emReportTable-tblBody', 30000); //Wait for filter label text 
    await browser.expect.element('#em--emReportTable-tblBody').text.to.not.contain('No data'); //check results table is not empty
});

When('I open ALL TMA', async function(){
    await browser.waitForElementPresent('#__filter0-__xmlview0--gogoFragment--header-5', 50000); //Wait for emergency Tab
    await browser.click(`#__filter0-__xmlview0--gogoFragment--header-5`); //Click the emergency tab
});

Then('ALL TMA Report should open', async function(){
    await browser.waitForElementPresent('#tma--tmaReportTable', 30000); //Wait for filter label text 
    await browser.expect.element('#tma--tmaReportTable').text.to.not.contain('No data'); //check results table is not empty
    //.end();  
});
/* - - - - - */

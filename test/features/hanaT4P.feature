Feature: OI Reports Test Automation - PROD
    Automation Testing of the ABSTUDY OI Reports in HANA PROD
@UnitTestPROD
    Scenario: HANA Login PROD
        Given I open HANA PROD Login
        Then I login to PROD
@UnitTestPROD
    Scenario: PROD Check
        Given I open HANA PROD
        When I click the Inbox tile
        Then I should see the inbox screen
@UnitTestPROD
    Scenario: Load all reports
        When I click the Outstanding button
        Then Outstanding Report should open
        Then PROD Outstanding Report should display results
        When I click the Reimbursement button
        Then Reimbursement Report should open
        Then PROD Reimbursement Report should display results
        When I open No Show Report
        Then No Show Report should open
        Then PROD No Show Report should display results
        When I open Quality Report
        Then Quality Report should open
        Then PROD Quality Report should display results
        When I open Emergency Report
        Then the Emergency Report should open
        Then PROD Emergency Report should display results
        When I open ALL TMA
        Then ALL TMA Report should open
        Then PROD All TMA Report should display results

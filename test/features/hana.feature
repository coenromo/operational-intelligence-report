Feature: OI Reports Test Automation - QAS
    Automation Testing of the ABSTUDY OI Reports in HANA QAS
@UnitTestQAS
    Scenario: HANA Login
        Given I open HANA
        Then I login
@UnitTestQAS
    Scenario: QAS Check
        Given I open HANA QAS
        When I click the Inbox tile
        Then I should see the inbox screen
@UnitTestQAS
    Scenario: Load all reports
        When I click the Outstanding button
        Then Outstanding Report should open
        Then Outstanding Report should display results
        When I click the Reimbursement button
        Then Reimbursement Report should open
        Then Reimbursement Report should display results
        When I open No Show Report
        Then No Show Report should open
        Then No Show Report should display results
        When I open Quality Report
        Then Quality Report should open
        Then Quality Report should display results
        When I open Emergency Report
        Then the Emergency Report should open
        Then Emergency Report should display results
        When I open ALL TMA
        Then ALL TMA Report should open
        Then All TMA Report should display results

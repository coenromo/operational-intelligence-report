/* global process, process */
const chromedriver = require('chromedriver');

module.exports = {
    silent: !process.env.NIGHTWATCH_VERBOSE,
    test_settings: {
        default: {
            webdriver: {
                start_process: true,
                port: 4444
            },
            screenshots: {
                enabled: true,
                path: 'report/screenshots'
            }
        },
        chromeHeadless: {
            webdriver: {
                server_path: chromedriver.path,
                cli_args: ['--port=4444']
            },
            desiredCapabilities: {
                browserName: 'chrome',
                javascriptEnabled: true,
                //loggingPrefs: { 'browser': 'ALL' },
                acceptSslCerts: true,
                chromeOptions: {
                    args: ['headless', 'disable-gpu', 'no-sandbox', 'proxy-server=http://proxy:0000/',
                        'incognito', 'ignore-certificate-errors',
                        'disable-infobars', 'allow-running-insecure-content', 'aggressive-cache-discard',
                        'disable-popup-blocking', 'disable-notifications', 'disable-session-crashed-bubble', 'window-size=1920,1080'
                    ]
                }
            }
        },
        chrome: {
            webdriver: {
                server_path: chromedriver.path,
                cli_args: ['--port=4444']
            },
            desiredCapabilities: {
                browserName: 'chrome',
                javascriptEnabled: true,
                acceptSslCerts: true,
                chromeOptions: {
                    args: ['disable-gpu', 'start-fullscreen']
                }
            }
        }
    }
};

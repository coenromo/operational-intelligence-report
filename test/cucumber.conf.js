/* global process, __dirname */
const fs = require('fs');
const path = require('path');
const {
    setDefaultTimeout,
    Before,
    After,
    AfterAll,
    BeforeAll
} = require('cucumber');
const {
    createSession,
    closeSession,
    startWebDriver,
    stopWebDriver
} = require('nightwatch-api');
const browser = require('nightwatch-api').client;
const reporter = require('cucumber-html-reporter');

const attachedScreenshots = getScreenshots();

function getScreenshots() {
    try {
        const folder = path.resolve(__dirname, 'report/screenshots');
        const screenshots = fs.readdirSync(folder).map(file => path.resolve(folder, file));
        return screenshots;
    } catch (err) {
        return [];
    }
}

setDefaultTimeout(180000);

BeforeAll(async () => {
    await startWebDriver({
        env: process.env.NIGHTWATCH_ENV || 'chrome'
    });
    await createSession();

    process.on('unhandledRejection', (reason, p) => {
        console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
        console.log(reason.stack);
    });

});

Before(function() {});

AfterAll(async () => {
    await closeSession();
    await stopWebDriver();
    setTimeout(() => {
        reporter.generate({
            theme: 'bootstrap',
            jsonFile: 'report/cucumber_report.json',
            output: 'report/cucumber_report.html',
            reportSuiteAsScenarios: true,
            launchReport: false,
            metadata: {
                'App Version': '0.0.1',
                'Test Environment': 'local'
            }
        });
    }, 1000);
});

After(async function() {
    await browser.getLog('browser', function(logEntriesArray) {
        console.log('\nBrowser Errors & Warnings:');
        logEntriesArray.forEach(function(log) {
            if (log.level === 'SEVERE' || log.level === 'WARNING') {
                console.log('[' + log.level + '] ' + log.timestamp + ' : ' + log.message);
            }
        });
        console.log('- - - - -\n');
    });

    return Promise.all(
        getScreenshots()
        .filter(file => !attachedScreenshots.includes(file))
        .map(file => {
            attachedScreenshots.push(file);
            return this.attach(fs.readFileSync(file), 'image/png');
        })
    );
});

# Operational Intelligence Report

OI Reports for a welfare payment to determine customers location and circumstance data at any given time to aid in claim processing.
These reports were later used by service centre staff to track the location of ABSTUDY students who were travelling or at risk during the COVID pandemic, and directly led to a member of the project recieving a NAIDOC award within Services Australia.

![OI Image](img/OI.png)

## Why

Business we're using an excel spreadsheet to track customers locations on varying welfare payments while travelling for study. This was obviously a poor solution for a number of reasons.

## What

Myself and two other developers built these Operational Intelligence reports after liasing with key stakeholders and agreeing on a set of requirements for the project. 

We worked within a larger Agile/SAFe team of about 8 people while completing various other tasks.

### Data

* Was built in SAP HANA back-end using graphical calculation views to generate table objects and create joins, unions, aggregations and projections on data sources
* We then used these views to generate XSOData that can be displayed in the UI

### UI

* Was built using SAPUI5 architecture which uses the model-view-controller (MVC) development concept
* We used the SAPUI5 Library and custom theming applied to the HANA environment 

### Automated Tests

I created an automated test using Nghtwatch and Cucumber to check the status of these reports. This was created because as we periodically released the reports to production (using the MVP concept) they would quite randomly break. We had no way to check if these reports were failing other than manually navigating to the URL or waiting for business to identify it. The issues were mostly data related and were almost always due to replication and date issues occurring between SAP CRM and HANA. There were additional times where other teams would change the database structure in SAP CRM, and these issues would flow thorugh to HANA during data replication and break the tables (having identical data in two systems is another conversation that I won't get into here).

In hindsight I wouldn't have used a UI end-to-end testing framework to confirm the status of reports that mainly had issues with data, but it was a quick solution and one that I was somewhat familiar with. 

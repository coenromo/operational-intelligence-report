sap.ui.define([
	'dhs/oi/base/controller/Base.controller',
	'sap/ui/export/Spreadsheet',
    'sap/m/MessageToast',
    'sap/m/Dialog',
    'sap/m/Button',
    'sap/m/RadioButton',
    'sap/ui/model/Sorter'
], function(BaseController, Spreadsheet, MessageToast, Dialog, Button, RadioButton, Sorter) {
	return BaseController.extend("dhs.abt.base.controller.Base", {

		/*====================================================================*/
		/* Look upon my Base Controller, ye mighty, and despair.         	  */
		/*====================================================================*/

		/**
		 * Converts all filters to a case insensitive format
		 * @param {string} sKey          Key for filter (a field/column in the OData service)
		 * @param {string} sOperator     Filter Operator
		 * @param {string} sValue        Value being converted
		 * @returns {object} Filter      New Filter object
		 */
		caseInsensitiveFilter: function(sKey, sOperator, sValue) {
			sValue = sValue.replace("'", "''");
			sValue = sValue.replace("\"", "\"\"");
			return new sap.ui.model.Filter("toupper(" + sKey + ")", sOperator, "'" + sValue.toUpperCase() + "'");
		},

	

        /**
         * Creates Columns for Spreadsheet
         * @param {string} sTab             Key of active tab
         * @returns {array} aColumns        Array of Column names and properties from reportColumns.json 
         */
		createColumns: function(sTab) {
			var aColumns = [];
			var columnModel = this.getView().getModel("export");

			aColumns = columnModel.getProperty("/Reports/" + sTab);

			return aColumns;
		},

        /**
         * Handles the 'Export to Excel' Button functionality
         * @returns {void}
         */
		onPressExport: function() {
			var sTab = this.byId("gogoFragment").getSelectedKey();
			var aColumns = this.createColumns(sTab);
			var oRowBinding, oSettings, oSheet, oTable;

			//Loop through aColumns, replacing Date function string with correct format.
			for (let i in aColumns) {
				if (aColumns[i].type === "sap.ui.export.EdmType.Date") {
					aColumns[i].type = sap.ui.export.EdmType.Date;
				} else if (aColumns[i].type === "sap.ui.export.EdmType.Currency") {
					aColumns[i].type = sap.ui.export.EdmType.Currency;
				} else if (aColumns[i].type === "sap.ui.export.EdmType.DateTime"){
				    aColumns[i].type = sap.ui.export.EdmType.DateTime;
				}
			}

			//replace / in the dateexport filename with _
			let sFullDate = new Date().toLocaleDateString("en-AU").replace("(///g, " - ")");

			//Set Filename based on report in context
			var excelFn = this.getView().getModel("oiConfig").getProperty("/Reports/" + sTab + "/Export");
			if(!excelFn){
			    excelFn = this.getView().getModel("oiConfig").getProperty("DefaultExport");
			}
			
			


			//Get binding to Report Table
			oTable = sap.ui.core.Fragment.byId(sTab, sTab + "ReportTable");

			oRowBinding = oTable.getBinding("items");

			//Get Rowbinding model interface
			var oModel = oRowBinding.getModel();
			var oModelInterface = oModel.getInterface();

			//Create Settings for Spreadsheet
			oSettings = {
				workbook: {
					columns: aColumns,
					hierarchyLevel: 'Level'
				},
				dataSource: {
					type: "odata",
					dataUrl: oRowBinding.getDownloadUrl ? oRowBinding.getDownloadUrl() : null,
					serviceUrl: oModelInterface.sServiceUrl,
					headers: oModelInterface.getHeaders ? oModelInterface.getHeaders() : null,
					count: oRowBinding.getLength ? oRowBinding.getLength() : null,
					useBatch: oModelInterface.bUseBatch,
					sizeLimit: oModelInterface.iSizeLimit
				},
				fileName: excelFn + ' - ' + sFullDate
			};
			oSheet = new Spreadsheet(oSettings);
			oSheet.build().finally(function() {
			});
		},
        
        /**
         * Handles the settings button functionality - Creates sorting dialog
         * @param {object} oEvent        Sort button Event
         * @returns {void}
         */
		onSort: function(oEvent) {
			// get selected report folder from gogofragment  
			var sTab = this.byId("gogoFragment").getSelectedKey();
			// apply to sFrag to open correct sort dialog
			var sFrag = "dhs.abt.base.view." + sTab + "." + sTab + "SortDialog";
			var oView = this.getView();
			var dSort = oView.createId(sTab);

			if (!this._oSettingDialog) {
				// if SortDialog doesn't exist, create it
				this._oSettingDialog = sap.ui.xmlfragment(oView.getId(), sFrag, this);
				oView.addDependent(this._oSettingDialog);
				this._oSettingDialog.open();
			} else if (this._oSettingDialog.sId === dSort) {
				// if current tab = SortDialog ID then open the current SortDialog
				this._oSettingDialog.open();
			} else {
				// if sTab and SortDialog ID don't match, destroy the dialog and create a new one
				this._oSettingDialog.destroy();
				this._oSettingDialog = sap.ui.xmlfragment(oView.getId(), sFrag, this);
				oView.addDependent(this._oSettingDialog);
				this._oSettingDialog.open();
			}
		},
		
		/**
		 * Handles the OK button functionality (from sort dialogs)
		 * @param {object} oEvent        OK Button Event
		 * @returns {void}
		 */
		onSortConfirm: function(oEvent) {
			var aSorters = [], oTable;
			var oView = this.getView();

			//apply to correct report table.
			var sTab = this.byId("gogoFragment").getSelectedKey();
			oTable = sap.ui.core.Fragment.byId(sTab, sTab + "ReportTable");
			

			var oBinding = oTable.getBinding("items");
			var mParams = oEvent.getParameters();

			if (!mParams.sortItem) {
				return;
			}
			var sPath = oEvent.getParameter("sortItem").getKey();
			var bDescending = mParams.sortDescending;
            var oEntity = oTable.getBindingInfo("items").binding.oEntityType.property;
            var oProperty = oEntity.find(function(obj) { return obj.name === sPath; });
			
            // toupper everythign all strings I guess
			if (oProperty.type === "Edm.String") {
			    aSorters.push(new Sorter("toupper(" + sPath + ")", bDescending));
            } else {
                aSorters.push(new Sorter(sPath, bDescending));
            }
			oBinding.sort(aSorters);
		},
		
		onResultsUpdate: function(oEvent) {
            //setting results count
            var oTitle, aHeaderCols;
            var oSource = oEvent.getSource();
			var oBinding = oSource.getBinding("items");
			var len = oBinding.getLength();
			var sTab = this.getView().byId("gogoFragment").getSelectedKey();

            oTitle = sap.ui.core.Fragment.byId(sTab, "resultTitle");    //grabbing title for setting the results count.
			aHeaderCols = sap.ui.core.Fragment.byId(sTab, "headerTable").getColumns();
            
			if (len && (oSource.getId() === sTab + "--" + sTab + "ReportTable")) {
				oTitle.setText("Results (" + len + ")");
			} else {
				oTitle.setText("Results");
			}
			
			/* Due to out of date ui5 libraries we have two tables to allow for a stick header and toolbar
            as a part of this, the width of the columns in the tables can obviously be iffy. We will re-align them here */
			var aDataCols = oSource.getColumns();
			var iHeaderLen = aHeaderCols.length - 1;
			
			for (var i = 0; i < iHeaderLen; i++) {
                aHeaderCols[i].setWidth(window.getComputedStyle(aDataCols[i].getDomRef(), null).width);
			}

            // A scroll bar is added when growing. It is always 12px wide, cater for this then growing.
			if (oEvent.getParameter("reason") === "Filter") {
                aHeaderCols[iHeaderLen].setWidth("0px");
			} else {
			    aHeaderCols[iHeaderLen].setWidth("12px");
			}
		}
	});
});
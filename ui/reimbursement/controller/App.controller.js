sap.ui.define([
   "sap/ui/core/mvc/Controller"
], function (Controller) {
   return Controller.extend("dhs.oi.abstudyTravel.reimbursement.controller.App", {});
}); 
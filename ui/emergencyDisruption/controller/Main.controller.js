sap.ui.define([
    "dhs/abt/base/controller/Base.controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(Controller, Filter, FilterOperator) {
	return Controller.extend("dhs.oi.abstudyTravel.emergencyDisruption.controller.Main", {

		/*====================================================================*/
		/* Welcome to the Beautiful Main Controller of ABSTUDY OI Reports	  */	
		/*====================================================================*/

        /**
         * Initialisation of Base Controller.
         * Set Enter Key to trigger onFilterReport
         */
		onInit: function() {
			this.getView().attachBrowserEvent("keydown", (oEvent) => {
				if (event.keyCode === 13) {
					this.onFilterReport();
				}
			});
			
			var oModel = this.getOwnerComponent().getModel("oiConfig");
			var oGogo = this.byId("gogoFragment");
			oModel.attachEventOnce("requestCompleted", function(oEvent){
			    oGogo.fireSelect();
			    $.ajax({
						url: "/dhs/oi/main/data/services/xsjs/abstudyTravel/checkAccess.xsjs",
						success: function(oData) {
							oGogo.getItems()[4].setVisible(true);
						},
						error: function(oData) {
							oGogo.getItems()[4].setVisible(false);
						}
				});
			    
			    
			});
		},
		



        
        /**
		 * Adds report fragment to tab
		 * @param {object} oEvent   iconTabBar Event
		 * @returns {void}  
		 */
		gogoFragment: function(oEvent) {
			var oTab = oEvent.getParameter("selectedItem") ? oEvent.getParameter("selectedItem") : oEvent.getSource().getItems()[0]; 
			var sKey = oEvent.getSource().getSelectedKey();

			if (sKey && oTab && !oTab.getContent().length) {
				oTab.addContent(sap.ui.xmlfragment(sKey, this.getView().getModel("oiConfig").getProperty("/Reports/" + sKey + "/Fragment"), this));
				//initialising default filter
			    if(sKey === "outstandingTravelItineraries"){
			        this.onFilterReport();
			    }
			}
			
		},
        
        /**
         * Handles the filtering functionality
         * @param {object} oEvent    Filterbar Search Event
         */
		onFilterReport: function(oEvent) {
			var sTab = this.byId("gogoFragment").getSelectedKey();
			var fFilterBar = sap.ui.core.Fragment.byId(sTab, sTab + "FilterBar").getFilterItems();
			var aFilters = [];
			var aGroupFilters = [];

			//Building Filter Array
			for (var i = 0; i < fFilterBar.length; i++) {
				var oControl = fFilterBar[i].getControl();
				var oType = oControl.getMetadata().getName();
				var sField = fFilterBar[i].data("filterTo");
				var sField2 = fFilterBar[i].data("filterTwo");
				var sQuery = "";
				var sQuery2 = "";
                
                //Switch for each UI Element Type
				switch (oType) {
					case "sap.m.MultiInput":
						sQuery = oControl.getTokens();
						if (sQuery && sQuery.length === 1) {
							aFilters.push(this.caseInsensitiveFilter(sField, FilterOperator.Contains, sQuery[0].getText()));
						} else if (sQuery && sQuery.length > 0) {
							aGroupFilters = [];
							for (let i = 0; i < sQuery.length; i++) {
								aGroupFilters.push(this.caseInsensitiveFilter(sField, FilterOperator.Contains, sQuery[i].getText()));
							}
							aFilters.push(new sap.ui.model.Filter(aGroupFilters, false));
						}
						break;
                    
					case "sap.m.MultiComboBox":
						//sQuery = oControl.getProperty("selectedKeys");
						sQuery = oControl.getSelectedKeys();
						if (sQuery && sQuery.length === 1) {
							aFilters.push(this.caseInsensitiveFilter(sField, FilterOperator.Contains, sQuery[0]));
						} else if (sQuery && sQuery.length > 0) {
							aGroupFilters = [];
							for (let i = 0; i < sQuery.length; i++) {
								aGroupFilters.push(this.caseInsensitiveFilter(sField, FilterOperator.Contains, sQuery[i]));
							}
							aFilters.push(new sap.ui.model.Filter(aGroupFilters, false));
						}
						break;
					case "sap.m.Input":
						sQuery = oControl.getValue();
						if (sQuery) {
							aFilters.push(this.caseInsensitiveFilter(sField, FilterOperator.Contains, sQuery));
						}
						break;
					case "sap.m.Select":
						sQuery = oControl.getProperty("selectedKey");
						if (sQuery) {
							aFilters.push(this.caseInsensitiveFilter(sField, FilterOperator.Contains, sQuery));
						}
						break;
					case "sap.m.DatePicker":
						sQuery = oControl.getDateValue();;
						if (sQuery) {
							sQuery.setHours(12); //accounting for time zone offset.

							aFilters.push(new sap.ui.model.Filter(sField, FilterOperator.EQ, sQuery));
						}
						break;
					case "sap.m.DateRangeSelection":
						sQuery = oControl.getDateValue();
						sQuery2 = oControl.getSecondDateValue();
						if (sQuery && sQuery2) {
                            sQuery.setHours(12); //time zone offset
							sQuery2.setHours(12);							    
							
							aGroupFilters = [];
							aGroupFilters.push(new sap.ui.model.Filter(sField, FilterOperator.BT, sQuery, sQuery2));
							if (sField2) {
								aGroupFilters = [];
								aGroupFilters.push(new sap.ui.model.Filter(sField, FilterOperator.BT, sQuery, sQuery2));
								aGroupFilters.push(new sap.ui.model.Filter(sField2, FilterOperator.BT, sQuery, sQuery2));
								aFilters.push(new sap.ui.model.Filter(aGroupFilters, false));
							} else {
								aFilters.push(new sap.ui.model.Filter(sField, FilterOperator.BT, sQuery, sQuery2));
							}
						}
						break;
					default:
						break;
				}

			}
			
			//binding filter array
			oTable = sap.ui.core.Fragment.byId(sTab, sTab + "ReportTable");
			var oBinding = oTable.getBinding("items");
			if(aFilters.length === 0){
			    oBinding.filter(aFilters);
			} else{
			    oBinding.filter(new sap.ui.model.Filter(aFilters, true));
			}
		},

        /**
         * Handles suggestion of Education Institution Name
         * Gets suggestValue from Event and requests OData from reportConfig
         * Binds OData return to Suggestion Items
         * @param {object} oEvent    Suggestion Event
         */ 
		onSuggestEdName: function(oEvent) {
			var sTerm = oEvent.getParameter("suggestValue");
			var aFilters = [];

			if (sTerm) {
				aFilters.push(this.caseInsensitiveFilter("ED_INST_NAME", sap.ui.model.FilterOperator.Contains, sTerm));
			}
			if (!oEvent.getSource().getBinding("suggestionItems")) {
				oEvent.getSource().bindAggregation("suggestionItems", {
					path: "reportConfig>/educationName",
					template: new sap.ui.core.Item({
						text: "{reportConfig>ED_INST_NAME}"
					})
				});
			} 
			oEvent.getSource().getBinding("suggestionItems").filter(aFilters);
			
		},
		
		/**
         * Handles suggestion of Community
         * Gets suggestValue from Event and requests OData from reportConfig
         * Binds OData return to Suggestion Items
         * @param {object} oEvent    Suggestion Event
         */ 
		onSuggestCommunity: function(oEvent) {
			var sTerm = oEvent.getParameter("suggestValue");
			var aFilters = [];

			if (sTerm) {
				aFilters.push(this.caseInsensitiveFilter("RESIDENCE_CMNTY_NAME", sap.ui.model.FilterOperator.Contains, sTerm));
			}
			if (!oEvent.getSource().getBinding("suggestionItems")) {
				oEvent.getSource().bindAggregation("suggestionItems", {
					path: "reportConfig>/communityName",
					template: new sap.ui.core.Item({
						text: "{reportConfig>RESIDENCE_CMNTY_NAME}"
					})
				});
			} 
			oEvent.getSource().getBinding("suggestionItems").filter(aFilters);
			
		},
		/**
		 * Handles the Clearing of the filterbar
		 * Sets a cleared state for different UI components
		 * @param {object} oEvent    Clear Event
		 */
		onFilterbarClear: function(oEvent) {
			let aFuncs = ["setValue", "setSelectedKey", "setSelectedKeys", "setTokens"];
			for (let oControl of oEvent.getParameter("selectionSet")) {
				for (let sFunc of aFuncs) {
					if (oControl[sFunc]) {
					    if (oControl.getMetadata().getName() === "sap.m.MultiInput") {
						    oControl[sFunc]([]);
					    } else {
					        oControl[sFunc]("");
					    }
					}
				}
			}
			// restoring Requested & Rescheduled to Booking Status for Outstanding Itineraries.
			let sTab = this.byId("gogoFragment").getSelectedKey();
			if(sTab ==="outstandingTravelItineraries"){
			    let oControl = sap.ui.core.Fragment.byId(sTab, "bookingStatus");
		        oControl.setSelectedKeys(["REQUESTED","RESCHEDULED"]);
			}
		}
	});
});
jQuery.sap.registerResourcePath('dhs/abt/base', '/dhs/oi/main/ui/components/abstudyTravel/base/');
jQuery.sap.registerResourcePath('dhs.abt.base', '/dhs/oi/main/ui/components/abstudyTravel/base/');



sap.ui.define([
	"dhs/oi/base/BaseComponent"
], function(BaseComponent) {

	return BaseComponent.extend("dhs.oi.abstudyTravel.emergencyDisruption.Component", {
		
		/*====================================================================*/
		/* Setup 															  */
		/*====================================================================*/
		
		metadata: {
			manifest: "json",
			config: {
				titleResource: "appTitle",
				resourceBundle: "i18n/i18n.properties"
			}
		},
		
		/**
		 * Initialise component.
		 */
		init: function() {
			//Call base component's init function
			BaseComponent.prototype.init.apply(this, arguments);
			// Configure models using dataSources defined in manifest
			this.configureModelsUsingDataSources().then(() => {
				// Initialise routing and navigation once models configured
				this.getRouter().initialize();
			});
		}
		
	});
});
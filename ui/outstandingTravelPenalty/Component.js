sap.ui.define([
	"sap/ui/core/UIComponent"
], function(UIComponent) {

	return UIComponent.extend("dhs.oi.abstudyTravel.outstandingTravelPenalty.Component", {
		
		/*====================================================================*/
		/* Setup 															  */
		/*====================================================================*/
		
		metadata: {
			manifest: "json",
			config: {
				titleResource: "appTitle",
				resourceBundle: "i18n/i18n.properties"
			}
		},
		
		/**
		 * Initialise component.
		 */
		init: function() {
			// Call base component's init function
			UIComponent.prototype.init.apply(this, arguments);
			// Initialise routing and navigation
			this.getRouter().initialize();
		}
		
	});
});